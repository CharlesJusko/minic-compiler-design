#include "Expressions.hpp"

#include <iostream>
#include <iomanip>

void Boolean_Expr::to_sexpr() const {
	std::cout << std::boolalpha << get_Value();
}

void Int_Expr::to_sexpr() const {
	std::cout << get_Value();
}

static void print_Sexpr(Binary_Expr const* expr, char const* operator) {
	std::cout << '(' << operator << ' ';
	expr->operands[0]->to_sexpr();
	std::cout << ' ';
	expr->operands[1]->to_sexpr();
	std::cout << ' ';
}

void Add_Expr::to_sexpr() const {
	print_Sexpr(this, '+');
}

void Sub_Expr::to_sexpr() const {
	print_Sexpr(this, '-');
}

void Mul_Expr::to_sexpr() const {
	print_Sexpr(this, '*');
}

void Div_Expr::to_sexpr() const {
	print_Sexpr(this, '/');
}

void Mod_Expr::to_sexpr() const {
	print_Sexpr(this, '%');
}
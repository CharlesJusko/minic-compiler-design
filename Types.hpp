#include <vector>
#include <iostream>

struct Type {
	enum Kind {
		bool_t,
		int_t,
		float_t,
		ref_t,
		fun_t
	};

	Type(Kind k) : kind(k)
	{}

	Kind kind;

	bool isBoolean() const { return kind == bool_t; }

	bool isInteger() const { return kind == int_t; }

	bool isFloat() const { return kind == float_t; }

	bool isReference() const { return kind == ref_t; }

	bool isFunction() const { return kind == fun_t; }
};

struct Bool_Type : Type {
	Bool_Type() : Type(bool_t)
	{}
};

struct Int_Type : Type {
	Int_Type() : Type(int_t)
	{}
};

struct Float_Type : Type {
	Float_Type() : Type(float_t)
	{}
};

struct Ref_Type : Type {
	Ref_Type(Type* t)
	  : Type(ref_t), t_ptr(t)
	{}

	Type* t_ptr;
};

struct Function_Type : Type {
	Function_Type(std::vector<Type*>&& vec)
	 : Type(fun_t), types(std::move(vec))
	{}

	std::vector<Type*>&& types;
};

std::ostream& operator<<(std::ostream& os, Type const& t);

void print(std::ostream& os, Type const* t);
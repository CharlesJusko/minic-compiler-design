#include "Tokens.hpp"

struct Lexer {

	Lexer(SymbolTable& st, std::string cosnt& s);

	Lexer(SymbolTable& st, char const* f, char const* e);

	SymbolTable* sTable;

	char const* first, end;

	int curLine;

	std::unordered_map<std::string, Token::Name> keywords;

	Token getNextToken();

	bool endOfFile(char const* limit) const { return limit == end; }

	bool endOfFile() const { return first == end; }

	char peek() const;

	char search(int n) const;

	char consume() { return *first++; }

	Token match(Token::Name n, int size);
};
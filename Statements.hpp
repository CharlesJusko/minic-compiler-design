#include<vector>
#include<iostream>
#include<array>

class Expr;
class Decl;
class Value;

struct Stmt {
	enum Kind {
		break_stmt,
		cont_stmt,
		block_stmt,
		while_stmt,
		if_stmt,
		ret_stmt,
		expr_stmt,
		decl_stmt
	};

	Stmt(Kind k) : kind(k)
	{}

	Kind getKind() const { return kind; }

	Kind kind;
};

struct Nullary_Stmt : Stmt {
	Nullary_Stmt(Kind k) : Stmt(k)
	{}
};

struct Unary_Stmt : Stmt {

	Unary_Stmt(Kind k, Stmt* s1)
	 : Stmt(k), stmnt(s1)
	 {}

	Stmt* stmnt;
};

struct Binary_Stmt : Stmt {

	Binary_Stmt(Kind k, Stmt* s1, Stmt* s2)
	 : Stmt(k), stmnts{s1, s2}
	{}

	std::array<Stmt*, 2> stmnts;
};

struct Kary_Stmt : Stmt {

	Kary_Stmt(Kind k, std::vector<Stmt*> vec)
	 : Stmt(k), stmnts(std::move(vec))
	 {}

	std::vector<Stmt*>&& stmnts;
};

// Nullary Statements

struct Break_Stmt : Nullary_Stmt {
	Break_Stmt() : Nullary_Stmt(break_stmt)
	{}
};

struct Continue_Stmt : Nullary_Stmt {
	Continue_Stmt() : Nullary_Stmt(cont_stmt)
	{}
};

struct Return_Stmt : Nullary_Stmt {
	Return_Stmt(Value* v) : Nullary_Stmt(ret_stmt), value(v)
	{}

	Value* value;
};

struct Expr_Stmt : Nullary_Stmt {
	Expr_Stmt(Expr* e1) : Nullary_Stmt(expr_stmt), expr(e1)
	{}

	Expr* expr;
};

struct Decl_Stmt : Nullary_Stmt{
	Decl_Stmt(Decl* d) : Nullary_Stmt(decl_stmt), decl(d)
	{}

	Decl* decl;
};

// Unary Statements

struct While_Stmt : Unary_Stmt {
	While_Stmt(Expr* e, Stmt* s)
	 : Unary_Stmt(while_stmt, s), expr(e)
	{}

	Expr* expr;
};

// Binary Statements

struct If_Stmt : Binary_Stmt {
	If_Stmt(Expr* e, Stmt* s1, Stmt* s2)
	 : Binary_Stmt(if_stmt, s1, s2), expr(e)
	{}

	Expr* expr;
};

// Kary Statements

struct Block_Stmt : Kary_Stmt {
	Block_Stmt(std::vector<Stmt*> vec)
	 : Kary_Stmt(block_stmt, vec)
	{}

	std::vector<Stmt*> getChildren() { return vec; }
};

std::ostream& operator<<(std::ostream& os, Stmt const& s);

void print(std::ostream& os, Stmt const* s);
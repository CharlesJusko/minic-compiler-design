#include "Symbols.hpp"

struct Token {

	enum Name {

		eof, // end of file

		lbrace,
		rbrace,
		lparen,
		rparen,
		colon,
		semicolon,
		comma,
		arrow,

		plus,
		minus,
		star,
		slash,
		percent,
		question,
		equals,
		double_equals,
		bang_equals,
		less,
		greater,
		less_equals,
		greater_equals,

		and_kw,
		bool_kw,
		break_kw,
		continue_kw,
		else_kw,
		false_kw,
		fun_kw,
		if_kw,
		int_kw,
		not_kw,
		or_kw,
		ref_kw,
		return_kw,
		true_kw,
		var_kw,
		while_kw,

		int_literal,
		float_literal,

		identifier
	};

	Token(Name n, Symnbol s, Location = {}) : name(n), symbol(s), location()
	{}

	Name name;
	Symbol symbol;
	Location location;

	bool isKeyword() const;

	bool isLogicalOperator() const;

	Symbol getLexeme() const { return symbol; }
};

std::ostream& operator<<(std::ostream& os, Token const& t);
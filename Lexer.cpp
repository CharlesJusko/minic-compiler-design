#include "Lexer.hpp"
#include <stdexcept>
#include <cctype>

Lexer::Lexer(SymbolTable& st, std::string cosnt& s) {
	Lexer(st, s.data(), s.data() + s.size());
}

Lexer::Lexer(SymbolTable& st, char const* f, char const* e) {
	sTable =&st;
	first = f;
	end = e;

	keywords.emplace("and", Token::and_kw);
	keywords.emplace("bool", Token::bool_kw);
	keywords.emplace("break", Token::break_kw);
	keywords.emplace("continue", Token::continue_kw);
	keywords.emplace("else", Token::else_kw);
	keywords.emplace("false", Token::false_kw);
	keywords.emplace("fun", Token::fun_kw);
	keywords.emplace("if", Token::if_kw);
	keywords.emplace("int", Token::int_kw);
	keywords.emplace("not", Token::not_kw);
	keywords.emplace("or", Token::or_kw);
	keywords.emplace("ref", Token::ref_kw);
	keywords.emplace("return", Token::return_kw);
	keywords.emplace("true", Token::true_kw);
	keywords.emplace("var", Token::var_kw);
	keywords.emplace("while", Token::while_kw);
}

char Lexer::peek() const {

	if(endOfFile()) {
		return 0;
	}
	return *first;
}

char Lexer::search(int n) const {

	if(endOfFile()) {
		return 0;
	}
	return *(first + n);
}

Token Lexer::match(Token::Name n, int size) {

	std::string s(first, first + size);
	Symbol sym = sTable->get(s);
	Token token = Token(n, sym);

	first += size;

	return token;
}

Token Lexer::getNextToken() {

	while(!endOfFile()) {

		switch(peek()) {

			// white space
			case ' ':
				consume();
				continue;
			case '\t':
				consume();
				continue;
			case '\n':
				++curLine;
				consume();
				continue;

			// comments - do nothing. The new line character will increment curLine
			case '#':
				while(peek() != '\n') {
					consume();
				}

			case '{':
				return match(Token::lbrace, 1);
			case '}':
				return match(Token::rbrace, 1);
			case '(':
				return match(Token::lparen, 1);
			case ')':
				return match(Token::rparen, 1);
			case ',':
				return match(Token::comma, 1);
			case ';':
				return match(Token::semicolon, 1);
			case ':':
				return match(Token::colon, 1);
			case '+':
				return match(Token::plus, 1);
			case '-':
				return match(Token::minus, 1);
			case '*':
				return match(Token::star, 1);
			case '/':
				return match(Token::slash, 1);
			case '%':
				return match(Token::percent, 1);

			case '<':
				if(search(1) == '=') {
					return match(Token::less_equals, 2);
				}
				return match(Token::less, 1);
			case '>':
				if(search(1) == '=') {
					return match(Token::greater_equals, 2);
				}
				return match(Token::greater, 1);
			case '=':
				if(search(1) == '=') {
					return match(Token::double_equals, 2);
				}
				return match(Token::equals, 1);
			case '!':
				if(search(1) == '=') {
					return match(Token::bang_equals, 2);
				}
				throw std::runtime_error("Not a token: Expected '=' after character '!'");
				continue;

			default:
				if(isNondigit(*first)) {
					return word();

				} else if(isDigit(*first)) {
					return number();
				}

				throw std::runtime_error("Invalid character on line: " << curLine);
				consume();
				continue;
		}
	}
}

static bool isNondigit(char c) {
	return std::isalpha(c) || c == '_';
}

static bool isDigit(char c) {
	return std::isdigit(c);
}

static bool isHex(char c) {
	return std::isxdigit(c);
}

static Token word() {

	char const* iter = first + 1;

	while(!endOfFile(iter) && (isNondigit(*iter) || isDigit(*iter))) {
		iter++;
	}

	first = iter;

	std::string token(first, iter);
	Symbol s = sTable->get(token);

	auto tok = keywords.find(token);
	Token::Name name;

	if(tok == keywords.end()) {
		name = Token::identifier;

	} else {
		name = tok->second;
	}

	return Token(name, s);
}

static Token number() {

	char const* prev = first - 1;

	// Checks it fits with with the identifier naming conventions
	// or checks if it is part of variable assignment
	if(!isNondigit(*prev) || *prev != '=' || (*prev != ' ' && *(prev - 1) != '=' )) {
		throw std::runtime_error("Invalid identifier declaration on line" << curLine);
	}

	return word();
}
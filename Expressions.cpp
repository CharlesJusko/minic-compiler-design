#include "Expressions.hpp"
#include "Name.hpp"
#include "Declarations.hpp"
#include "Types.hpp"
#include "Value.hpp"

#include <iostream>

static void print_Str(std::ostream& os, char const* cstr) {
	os << cstr;
}

static void print_Bool(std::ostream& os, Boolean_Expr const* e) {
	
	if(e->value->getInteger() == 0) {
		os << "false";

	} else {
		os << "true";
	}
}

static void print_Int(std::ostream& os, Int_Expr const* e) {
	os << e->value->getInteger();
}

static void print_Float(std::ostream& os, Float_Expr const* e) {
	os << e->value->getFloat();
}

static void print_Id(std::ostream& os, Identifier_Expr const* e) {
	os << e->decl->name->str;
}

static void print_Unary(std::ostream& os, Unary_Expr const* e, char const* op) {
	
	os << op;
	print(os, e->operand);
}

static void print_Binary(std::ostream& os, Binary_Expr const* e, char const* op) {
	
	print(os, e->operands[0]);
	os << op;
	print(os, e->operands[1]);
}

static void print_Ternary(std::ostream& os, Ternary_Expr const* e) {

	os << "if ";
	print(os, e->operands[0]);
	os << " then ";
	print(os, e->operands[1]);
	os << " else ";
	print(os, e->operands[2]);
}

static void print_FunCall(std::ostream& os, Kary_Expr const* e) {

	auto size = e->operands.size() - 1;
	os << "(";
	for(int i = 0; i < size - 1; ++i) {
		print(os, e->operands[i]);
	}
	os << ")";
}

void print(std::ostream& os, Expr const* e) {

	switch (e->kind) {

		case Expr::int_expr:
			return print_Int(os, static_cast<Int_Expr const*>(e));
		case Expr::bool_expr:
			return print_Bool(os, static_cast<Boolean_Expr const*>(e));
		case Expr::float_expr:
			return print_Float(os, static_cast<Float_Expr const*>(e));
		case Expr::id_expr:
			return print_Id(os, static_cast<Identifier_Expr const*>(e));

		case Expr::not_expr:
			return print_Unary(os, static_cast<Unary_Expr const*>(e), "!");
		case Expr::neg_expr:
			return print_Unary(os, static_cast<Unary_Expr const*>(e), "-");
		case Expr::rec_expr:
			return print_Unary(os, static_cast<Unary_Expr const*>(e), "/");
		case Expr::convert_expr:
			return print_Unary(os, static_cast<Unary_Expr const*>(e), "val ");

		case Expr::add_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "+");
		case Expr::sub_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "-");
		case Expr::mul_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "*");
		case Expr::div_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "/");
		case Expr::mod_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "%");

		case Expr::and_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "&&");
		case Expr::or_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "||");

	 	case Expr::eq_expr:
	 		return print_Binary(os, static_cast<Binary_Expr const*>(e), "==");
		case Expr::ne_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "!=");
	 	case Expr::lt_expr:
	 		return print_Binary(os, static_cast<Binary_Expr const*>(e), "<");
		case Expr::gt_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), ">");
		case Expr::le_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "<=");
		case Expr::ge_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), ">=");
		case Expr::asmnt_expr:
			return print_Binary(os, static_cast<Binary_Expr const*>(e), "=");

		case Expr::cond_expr:
			return print_Ternary(os, static_cast<Ternary_Expr const*>(e));

		case Expr::call_expr:
			return print_FunCall(os, static_cast<Kary_Expr const*>(e));

	    break;
	
	}
}

std::ostream& operator<<(std::ostream& os, Expr const& e) {
	print(os, &e);
	return os;
}
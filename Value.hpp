#include <cassert>

struct Value {

	enum Kind
	{
		null_val,
		int_val,
		float_val,
		ref_val, // address
		fun_val
	};

	union Data {

		Data() = default;
		
		Data(int n) : num(n)
		{}

		Data(float d) : flt(d)
		{}

		Data(Fun_Decl* f) : fun(f)
		{}

		Data(Ref_Decl* r) : ref(r)
		{}

		int num;
		float flt;
		Fun_Decl* fun;
		Ref_Decl* ref;

	};

	Value() : kind(null_val)
	{}

	Value(int n) : kind(int_val), data(n)
	{}

	Value(bool b) : Value((int)b)
	{}

	Value(float f) : kind(float_val), data(f)
	{}

	Value(Fun_Decl* f) : kind(fun_val), data(f)
	{}

	Value(Ref_Decl* r) : kind(ref_val), data(r)
	{}

	Kind kind;

	Data data;

	bool isInit() const { return kind == null_val; }

	bool isInt() const { return kind == int_val; }

	bool isFloat() const { return kind == float_val; }

	bool isFunction() const { return kind == fun_val; }

	bool isReference() const { return kind == ref_val; }

	int getInteger() const;

	float getFloat() const;

	Fun_Decl* getFunction() const;

	Ref_Decl* getAddress() const; // Reference
};

inline int Value::getInteger() const {
	assert(isInt());
	return data.num;
}

inline float Value::getFloat() const {
	assert(isFloat());
	return data.flt;
}

inline Fun_Decl* Value::getFunction() const {
	assert(isFunction());
	return data.fun;
}

inline Ref_Decl* Value::getAddress() const {
	assert(isReference());
	return data.ref;
}

inline void print(std::ostream& os, Value const* v){
	
	if(v->isInt()){
		os << v->getInteger();

	} else if(v->isFloat()){
		os << v->getFloat();

	} else if(v->isReference()){
		os << v->getAddress();

	} else {
		assert(false);
	}
}
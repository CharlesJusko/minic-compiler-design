#include "Types.hpp"
#include "Expressions.hpp"
#include "Declarations.hpp"

Value evaluateExpr(Expr const* e);
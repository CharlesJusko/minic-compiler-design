set(CMAKE_CXX_FLAGS "-std=c++1z")

add_executable(mcc
	Expressions.cpp
	Types.cpp
	Declarations.cpp
	Statements.cpp
	ExprEvals.cpp
	StmntEvals.cpp
	Value.cpp
	Lexer.cpp
	Tokens.cpp
	Symbols.cpp
	Actions.cpp
	Parser.cpp
	main.cpp
)

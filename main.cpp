#include "Expressions.hpp"
#include "Types.hpp"
#include "Declarations.hpp"
#include "Statements.hpp"
#include "Value.hpp"
#include "Name.hpp"

#include <iostream>

Decl* tester() {

	Type* b = new Boolean_Type();
	Type* i = new Int_Type();

	Decl* a = new Obj_Decl(new Name("test1"), i, nullptr);
	Decl* b = new Obj_Decl(new Name("test2"), i, nullptr);

	Expr* expr = new Cond_Expr(
		i, new NotEqual_Expr(b, new Identifier_Expr(i, a), new Identifier_Expr(i, b)),
		new Identifier_Expr(i, a),
		new Identifier_Expr(i, b)
	);

	std::vector<Stmt*> v = {new Ret_Stmt(expr)};
	Stmt* body = new Block_Stmt(v);

	std::vector<Type*> types = {i, i, i};
	Type* f = new Function_Type(types);

	std::vector<Decl*> decs = {a, b};
	return new Fun_Decl(new Name("test"), f, decs, body);
}

int main() {

	Decl* test = tester();
	std::cout << *test << '/n';

}
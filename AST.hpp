#include <vector>
#include <cassert>

template<typename T>
struct Node {

	Node() : children() // nullary
	{} 

	Node(T* e) : children{e} // unary
	{}

	Node(T* e1, T* e2) : children{e1, e2} // binary
	{}

	Node(T* e1, T* e2, T* e3) : children{e1, e2, e3} // ternary
	{}

	Node(std::vector<T*> const& vec) : children(vec) // Node will have the children in vec
	{}

	std::vector<T*> children;

	// Methods

	T* get_Child(int index) const;
};

template<typename T>
inline T* Node<T>::get_Child(int index) const {

	assert(index >= 0 && index < children.size());
	return children[index];
}

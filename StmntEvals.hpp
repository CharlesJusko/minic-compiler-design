#include "Types.hpp"
#include "Declarations.hpp"

enum PostStmt {
	nextPs,
	breakPs,
	continuePs,
	returnPs
};

PostStmt evaluateStmt(Stmt const* e);
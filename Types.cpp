#include "Types.hpp"

#include <iostream>
#include <vector>

static void print_Str(std::ostream& os, char const* cstr) {
	os << cstr;
}

static void print_Ref(std::ostream& os, Ref_Type const* t) {
	os << "ref " << *t->t_ptr;
}

static void print_Fun(std::ostream& os, Function_Type const* t) {
	auto size = t->types.size() - 1;
	os << "(";
	for(int i = 0; i < size - 1; ++i) {
		print(os, t->types[i]);
	}
	os << ") -> ";
	print(os, t->types[size]);
}

void print(std::ostream& os, Type const* t) {
	switch (t->kind) {
		case Type::bool_t:
			return print_Str(os, "bool");

		case Type::int_t:
			return print_Str(os, "int");

		case Type::ref_t:
			return print_Ref(os, static_cast<Ref_Type const*>(t));

		case Type::fun_t:
			return print_Fun(os, static_cast<Function_Type const*>(t));
	}
}

std::ostream& operator<<(std::ostream& os, Type const& t) {
	print(os, &t);
	return os;
}

#include <iostream>

#include "Tokens.hpp"

static char const* lexeme(Token::Name n) {

	switch(n) {

		case Token::eof:
			return "eof";

		case Token::lbrace:
			return "lbrace";
		case Token::rbrace:
			return "rbrace";
		case Token::lparen:
			return "lparen";
		case Token::rparen:
			return "rparen";
		case Token::colon:
			return "colon";
		case Token::semicolon:
			return "semicolon";
		case Token::comma:
			return "comma";
		case Token::arrow:
			return "arrow";

		case Token::puls:
			return "plus";
		case Token::minus:
			return "minus";
		case Token::star:
			return "star";
		case Token::slash:
			return "slash";
		case Token::percent:
			return "percent";
		case Token::question:
			return "question";
		case Token::equals:
			return "equals";
		case Token::double_equals:
			return "double_equals";
		case Token::bang_equals:
			return "bang_equals";
		case Token::less:
			return "less";
		case Token::greater:
			return "greater";
		case Token::less_equals:
			return "less_equals";
		case Token::greater_equals:
			return "greater_equals";

		case Token::and_kw:
			return "and_kw";
		case Token::bool_kw:
			return "bool_kw";
		case Token::break_kw:
			return "break_kw";
		case Token::continue_kw:
			return "continue_kw";
		case Token::else_kw:
			return "else_kw";
		case Token::false_kw:
			return "false_kw";
		case Token::fun_kw:
			return "fun_kw";
		case Token::if_kw:
			return "if_kw";
		case Token::int_kw:
			return "int_kw";
		case Token::not_kw:
			return "not_kw";
		case Token::or_kw:
			return "or_kw";
		case Token::ref_kw:
			return "ref_kw";
		case Token::return_kw:
			return "return_kw";
		case Token::true_kw:
			return "true_kw";
		case Token::var_kw:
			return "var_kw";
		case Token::while_kw:
			return "while_kw";

		case Token::int_literal:
			return "int_literal";
		case Token::float_literal:
			return "float_literal";

		case Token::identifier:
			return "identifier";
	}
}

bool isKeyword() const {

	switch(this.name) {

		case Token::and_kw:
			return true;
		case Token::bool_kw:
			return true;
		case Token::break_kw:
			return true;
		case Token::continue_kw:
			return true;
		case Token::else_kw:
			return true;
		case Token::false_kw:
			return true;
		case Token::fun_kw:
			return true;
		case Token::if_kw:
			return true;
		case Token::int_kw:
			return true;
		case Token::not_kw:
			return true;
		case Token::or_kw:
			return true;
		case Token::ref_kw:
			return true;
		case Token::return_kw:
			return true;
		case Token::true_kw:
			return true;
		case Token::var_kw:
			return true;
		case Token::while_kw:
			return true;

		default:
			return false;
	}
}

bool isLogicalOperator() const {

	switch(this.name) {

		case Token::puls:
			return true;
		case Token::minus:
			return true;
		case Token::star:
			return true;
		case Token::slash:
			return true;
		case Token::percent:
			return true;
		case Token::question:
			return true;
		case Token::equals:
			return true;
		case Token::double_equals:
			return true;
		case Token::bang_equals:
			return true;
		case Token::less:
			return true;
		case Token::greater:
			return true;
		case Token::less_equals:
			return true;
		case Token::greater_equals:
			return true;

		default:
			return false;
	}
}

std::ostream operator<<(std::ostream& os, Token const& t) {
	return os << "<" << lexeme(t.name) << ">";
}
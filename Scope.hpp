#include <vector>
#include <unordered_map>

#include "Symbols.hpp"
#include "Declarations.hpp"

struct Scope : std::unordered_map<Symbol, Decl*> {

	Decl* search(Symbol s) {

		auto iter = find(s);

		if(iter == end()) {
			return nullptr;
		}

		return iter->second;
	}

	void declare(Decl* d) {
		assert(count(d) == 0);
		emplace(d->name, d);
	}
};

struct ScopeStack : std::vector<Scope> {

	Decl* search(Symbol s) {

		for(auto iter = rbegin(); iter != rend(); iter++) {

			if(Decl* d = iter->search(s)) {

				return d;
			}
		}

		return nullptr;
};
#include "Declarations.hpp"
#include "Name.hpp"
#include "Types.hpp"
#include "Expressions.hpp"
#include "Statements.hpp"

#include <iostream>

static void print_Obj(std::ostream& os, Obj_Decl const* d) {
	
	os << "var ";
	os << d->name->str;

	print(os, d->type);

	if(Expr const* e = d->init) {
		print(os, d->init);
	}
}

static void print_Ref(std::ostream& os, Ref_Decl const* d) {

	os << "ref ";
	print(os, d->name);
	print(os, d->type);

	if(Expr const* e = d->init) {
		print(os, d->init);
	}
}

static void print_Fun(std::ostream& os, Fun_Decl const* d) {
	
	os << "fun ";
	print(os, d->name);
	print(os, d->type);

	os << "(";
	for(Decl* arg : d->args) {
		print(os, arg);
	}
	os << ")";
}

void print(std::ostream& os, Decl const* d) {

	switch(d->kind) {

		case Decl::obj_decl:
			return print_Obj(os, static_cast<Obj_Decl const*>(d));

		case Decl::ref_decl:
			return print_Ref(os, static_cast<Ref_Decl const*>(d));

		case Decl::fun_decl:
			return print_Fun(os, static_cast<Fun_Decl const*>(d));
	}
}

std::ostream& operator<<(std::ostream& os, Decl const& d) {
	print(os, &d);
	return os;
}
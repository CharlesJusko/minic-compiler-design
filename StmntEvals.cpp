#include "StmntEvals.hpp"
#include "ExprEvals.hpp"

#include <iostream>
#include <cassert>

static PostStmt evaluateBreak(Break_Stmt const* s) {
	return breakPs;
}

static PostStmt evaluateCont(Continue_Stmt const* s) {
	return continuePs;
}

static PostStmt evaluateBlock(Block_Stmt const* s) {

	for(Stmt* stmt : s->getChildren()) {
		PostStmt ps = evaluateStmt(stmt);

		if(ps != nextPs) {
			return ps;
		}
	}

	return nextPs
}

static PostStmt evaluateWhile(While_Stmt const* s) {

	bool flag = true;
	while(flag) {

		Value condition = evaluateExpr(s->expr);

		if(condition.getInteger()) {
			PostStmt ps = evaluateStmt(s->stmt);

			if(ps == returnPs) {
				return ps;

			} else if(ps == continuePs) {
				continue;

			} else if(ps == breakPs) {
				break;
			}

		} else {
			flag = false;
		}
	}

	return nextPs;
}

static PostStmt evaluateIf(If_Stmt const* s) {

	Value condition = evaluateExpr(s->expr);

	if(condition.getInteger()){
		return evaluateStmt(s->s1);

	} else {
		return evaluateStmt(s->s2);
	}
}

static PostStmt evaluateReturn(Return_Stmt const* s) {

	Value val = evaluateExpr(s->val);

	return returnPs
}

static PostStmt evaluateExpr(Expr_Stmt const* s) {
	evaluateExpr(s->expr);
	return nextPs;
}

static PostStmt evaluateDecl(Decl_Stmt const* s) {

	Value val;

	if(s->decl->kind == obj_decl) {
		Obj_Decl* obj = static_cast<Obj_Decl*>(s->decl);
		val = evaluateExpr(obj->init);

	} else if(s->decl->kind == ref_decl) {
		Ref_Decl* ref = static_cast<Ref_Decl*>(s->decl);
		val = evaluateExpr(ref->init);
	}

	return nextPs;
}

PostStmt evaluateStmt(Stmt const* s) {

	switch(s->kind) {

		case Stmt::break_stmt:
			return evaluateBreak(static_cast<Break_Stmt const*>(s));
		case Stmt::cont_stmt:
			return evaluateCont(static_cast<Continue_Stmt const*>(s));
		case Stmt::block_stmt:
			return evaluateBlock(static_cast<Break_Stmt const*>(s));
		case Stmt::while_stmt:
			return evaluateWhile(static_cast<While_Stmt const*>(s));
		case Stmt::if_stmt:
			return evaluateIf(static_cast<If_Stmt const*>(s));
		case Stmt::ret_stmt:
			return evaluateReturn(static_cast<Return_Stmt const*>(s));
		case Stmt::expr_stmt:
			return evaluateExpr(static_cast<Expr_Stmt const*>(s));
		case Stmt::decl_stmt:
			return evaluateDecl(static_cast<Decl_Stmt const*>(s));
	}

	assert(false);
}
#include<vector>
#include<iostream>

class Name;
class Type;
class Expr;
class Stmt;

struct Decl {
	enum Kind {
		obj_decl,
		ref_decl,
		fun_decl
	};

	Decl(Kind k) : kind(k)
	{}

	Decl(Kind k, Name* n, Type* t)
	 : kind(k), name(n), type(t)
	 {}

	Kind kind;
	Name* name;
	Type* type;

	bool isObject() const { return kind == obj_decl; }

	bool isReference() const { return kind == ref_decl; }

	bool isFunction() const { return kind == fun_decl; }
};

struct Obj_Decl : Decl {

	Obj_Decl(Name* n, Type* t, Expr* e)
	 : Decl(obj_decl, n, t), init(e)
	{}

	Expr* init;
};

struct Ref_Decl : Decl {
	
	Ref_Decl(Name* n, Type* t, Expr* e)
	 : Decl(ref_decl, n, t), init(e)
	{}

	Expr* init;
};

struct Fun_Decl : Decl {

	Fun_Decl(Name* n, Type* t, std::vector<Decl*>&& vec, Stmt* s)
	 : Decl(fun_decl, n, t), args(std::move(vec)), body(s)
	{}

	std::vector<Decl*>&& args;
	Stmt* body;
};

std::ostream& operator<<(std::ostream& os, Decl const& d);

void print(std::ostream& os, Decl const* d);
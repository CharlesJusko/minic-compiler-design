#include "ExprEvals.hpp"
#include "Value.hpp"

#include <iostream>
#include <cassert>
#include <functional>

static Value evaluateLit(Literal_Expr const* e) {
	return e->value;
}

static Value evaluateId(Identifier_Expr const* e) {

	if((e->decl)->isFunction()) {
		return Value(static_cast<Fun_Decl*>(e->decl));

	} else if((e->decl)->isObject()) {
		return Value(static_cast<Obj_Decl*>(e->decl));
	}
}

template<typename Opr>
static Value evaluateUnary(Unary_Expr const* e, Opr opr) {

	Value val = evaluateExpr(e->operand);

	if(e->type->isInteger()) {
		return Value(opr(val.getInteger()));

	} else if(e->type->isFloat()) {
		return Value(opr(val.getFloat()));

	} else {
		assert(false);
	}
}

static Value evaluateValConv(ValConversion_Expr const* e) {

	Value val = evaluateExpr(e->operand);
	return val;
}

template<typename T = void>
struct invert {
	T operator()(T const& num) const { return T(1) / num; }
};

template<typename Opr>
static Value evaluatBinary(Binary_Expr const* e, Opr opr) {

	Value v1 = evaluateExpr(e->operands[0]);
	Value v2 = evaluateExpr(e->operands[1]);

	if(e->type->isInteger()) {
		return Value(opr(v1.getInteger(), v2.getInteger()));

	} else if(e->type->isFloat()) {
		return Value(opr(v1.getFloat(), v2.getFloat()));

	} else {
		assert(false);
	}
}

static Value evaluateAssign(Assign_Expr const* e) {

	Value v1 = evaluateExpr(e->operands[0]);
	Value v2 = evaluateExpr(e->operands[1]);

	v1 = v2;

	return v1;
}

static Value evaluateCond(Cond_Expr const* e) {

	Value condition = evaluateExpr(e->operands[0]);

	if(condition.getInteger()) {
		return evaluateExpr(e->operands[1]);

	} else {
		return evaluateExpr(e->operands[2]);
	}
}

static Value evaluateFun(Call_Expr const* e) {

	Value func = evaluateExpr(e->operands);

	Fun_Decl* fdecl = func.getFunction();

	evaluateStmt(fdecl->body);

	Value retVal = evaluateExpr(e->operands.back());

	return retVal;
}

Value evaluateExpr(Expr const* e) {

	switch(e->kind) {

		// Literals
		case Expr::int_expr:
			return evaluateLit(static_cast<Literal_Expr const*>(e));
		case Expr::bool_expr:
			return evaluateLit(static_cast<Literal_Expr const*>(e));
		case Expr::float_expr:
			return evaluateLit(static_cast<Literal_Expr const*>(e));

		// Id
		case Expr::id_expr:
			return evaluateId(static_cast<Identifier_Expr const*>(e));

		// Unary
		case Expr::not_expr:
			return evaluateUnary(static_cast<Unary_Expr const*>(e), std::logical_not<>{});
		case Expr::neg_expr:
			return evaluateUnary(static_cast<Unary_Expr const*>(e), std::negate<>{});
		case Expr::rec_expr:
			return evaluateUnary(static_cast<Unary_Expr const*>(e), invert<>{});
		case Expr::convert_expr:
			return evaluateValConv(static_cast<ValConversion_Expr const*>(e));
		
		// Binary
		case Expr::and_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::logical_and<>{});
		case Expr::or_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::logical_or<>{});
		case Expr::eq_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::equal_to<>{});
		case Expr::ne_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::not_equal_to<>{});
		case Expr::lt_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::less<>{});
		case Expr::gt_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::greater<>{});
		case Expr::le_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::less_equal<>{});
		case Expr::ge_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::greater_equal<>{});
		case Expr::add_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::plus<>{});
		case Expr::sub_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::minus<>{});
		case Expr::mul_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::multiplies<>{});
		case Expr::div_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::divides<>{});
		case Expr::mod_expr:
			return evaluatBinary(static_cast<Binary_Expr const*>(e), std::modulus<>{});
		case Expr::asmnt_expr:
			return evaluateAssign(static_cast<Assign_Expr const*>(e));

		// Ternary
		case Expr::cond_expr:
			return evaluateCond(static_cast<Cond_Expr const*>(e));

		// Kary/Function Call
		case Expr::fun_expr:
			return evaluateFun(static_cast<Call_Expr const*>(e));

	}
}
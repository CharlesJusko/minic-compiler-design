#include <string>
#include <unordered_set>

struct Symbol {

	Symbol(std::string const* s) : symbol(s)
	{}

	std::string const* symbol;

	bool operator==(Symbol s1, Symbol s2) { return s1.symbol == s2.symbol; }

	bool operator!=(Symbol s1, Symbol s2) { return s1.symbol != s2.symbol; }

	std::string const& lexeme() const { return *symbol; }
};

struct SymbolTable : std::unordered_set<std::string> {

	Symbol get(std::string const& s);

	Symbol get(char const* s);
};

inline Symbol SymbolTable::get(std::string const& s) {
	return &*emplace(s).first;
}

inline Symbol SymbolTable::get(char const* s) {
	return &*emplace(s).first;
}
#include <vector>
#include <iostream>

class Value;
class Decl;
class Type;

struct Expr {
	enum Kind {

		// Nullary
		int_expr,
		bool_expr,
		float_expr,
		id_expr,

		// Unary
		not_expr,
		neg_expr,
		rec_expr,
		convert_expr,

		// Binary
		and_expr,
		or_expr,
		eq_expr,
		ne_expr,
		lt_expr,
		gt_expr,
		le_expr,
		ge_expr,
		add_expr,
		sub_expr,
		mul_expr,
		div_expr,
		mod_expr,
		asmnt_expr,

		// Ternary
		cond_expr,

		// Kary
		call_expr
	};

	Expr(Kind k, Type* t) : kind(k), type(t)
	{}

	Kind kind;

	Type* type;
};

struct Nullary_Expr : Expr {

	Nullary_Expr(Kind k, Type* t)
	 : Expr(k, t)
	 {}
};

struct Unary_Expr : Expr { 

	Unary_Expr(Kind k, Type* t, Expr* e1)
	 : Expr(k, t), operand(e1)
	{}

	Expr* operand;
};

struct Binary_Expr : Expr {

	Binary_Expr(Kind k, Type* t, Expr* e1, Expr* e2)
	 : Expr(k, t), operands{e1, e2}
	{}

	std::vector<Expr*> operands;
};

struct Ternary_Expr : Expr {

	Ternary_Expr(Kind k, Type* t, Expr* e1, Expr* e2, Expr* e3)
	 : Expr(k, t), operands{e1, e2, e3}
	{}

	std::vector<Expr*> operands;
};

struct Kary_Expr : Expr {

	Kary_Expr(Kind k, Type* t, std::vector<Expr*> vec)
	 : Expr(k, t), operands(vec)
	{}

	std::vector<Expr*> operands;
};

// Literals/Nullary nodes with values

struct Literal_Expr : Nullary_Expr { 

	Literal_Expr(Kind k, Type* t, Value* val)
	 : Nullary_Expr(k, t), value(val)
	{}

	Value* value;
};

// Integer Literals
struct Int_Expr : Literal_Expr {
	Int_Expr(Type* t, Value* val)
	 : Literal_Expr(int_expr, t, val)
	{}
};

// Boolean Literals
struct Boolean_Expr : Literal_Expr {
	Boolean_Expr(Type* t, Value* val)
	 : Literal_Expr(bool_expr, t, val)
	{}
};

// Float Literals
struct Float_Expr : Literal_Expr {
	Float_Expr(Type* t, Value* val)
	 : Literal_Expr(float_expr, t, val)
	{}
};

// Variable Identifiers
struct Identifier_Expr : Nullary_Expr {
	Identifier_Expr(Type* t, Decl* d)
	 : Nullary_Expr(id_expr, t), decl(d)
	{}

	Decl* decl;
};

// Unary Expressions, one operand

struct Not_Expr : Unary_Expr {
	Not_Expr(Type* t, Expr* e1)
	 : Unary_Expr(not_expr, t, e1)
	 {}
};

struct Negate_Expr : Unary_Expr {
	Negate_Expr(Type* t, Expr* e1)
	 : Unary_Expr(neg_expr, t, e1)
	 {}
};

struct Recip_Expr : Unary_Expr {
	Recip_Expr(Type* t, Expr* e1)
	 : Unary_Expr(rec_expr, t, e1)
	 {}
};

struct ValConversion_Expr : Unary_Expr {
	ValConversion_Expr(Type* t, Expr* e1)
	 : Unary_Expr(convert_expr, t, e1)
	 {}
};

// Binary Expressions, two operands

struct And_Expr : Binary_Expr {
	And_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(and_expr, t, e1, e2)
	{}
};

struct Or_Expr : Binary_Expr {
	Or_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(or_expr, t, e1, e2)
	{}
};

struct Equality_Expr : Binary_Expr {
	Equality_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(eq_expr, t, e1, e2)
	{}
};

struct NotEqual_Expr : Binary_Expr {
	NotEqual_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(ne_expr, t, e1, e2)
	{}
};

struct LessThan_Expr : Binary_Expr {
	LessThan_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(lt_expr, t, e1, e2)
	{}
};

struct GreaterThan_Expr : Binary_Expr {
	GreaterThan_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(gt_expr, t, e1, e2)
	{}
};

struct LessEq_Expr : Binary_Expr {
	LessEq_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(le_expr, t, e1, e2)
	{}
};

struct GreaterEq_Expr : Binary_Expr {
	GreaterEq_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(ge_expr, t, e1, e2)
	{}
};

struct Add_Expr : Binary_Expr { 
	Add_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(add_expr, t, e1, e2)
	{}
};

struct Sub_Expr : Binary_Expr {
	Sub_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(sub_expr, t, e1, e2)
	{}
};

struct Mul_Expr : Binary_Expr {
	Mul_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(mul_expr, t, e1, e2)
	{}
};

struct Div_Expr : Binary_Expr {
	Div_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(div_expr, t, e1, e2)
	{}
};

struct Mod_Expr : Binary_Expr {
	Mod_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(mod_expr, t, e1, e2)
	{}
};

struct Assign_Expr : Binary_Expr {
	Assign_Expr(Type* t, Expr* e1, Expr* e2)
	 : Binary_Expr(asmnt_expr, t, e1, e2)
	 {}
};

// Ternary Expressions, three operands
// Conditionals, takes the form of (e1 ? e2 : e3)
struct Cond_Expr : Ternary_Expr {
	Cond_Expr(Type* t, Expr* e1, Expr* e2, Expr* e3)
	 : Ternary_Expr(cond_expr, t, e1, e2, e3)
	 {}
};

// Kary Expressions, many operands
// Function calls, takes the form of e(e1, e2, ..., en)
struct Call_Expr : Kary_Expr {
	Call_Expr(Type* t, std::vector<Expr*> vec)
	 : Kary_Expr(call_expr, t, vec)
	 {}
};

std::ostream& operator<<(std::ostream& os, Expr const& e);

void print(std::ostream& os, Expr const* e);
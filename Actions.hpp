#include "Tokens.hpp"
#include "Expressions.hpp"
#include "Statements.hpp"
#include "Declarations.hpp"
#include "Types.hpp"
#include "Scope.hpp"

struct Actions {

	Expr* boolLiteral(Token const& token);

	Expr* intLiteral(Token const& token);

	Expr* idExpr(Token const& token);

	// Expressions
	Expr* negateExpr(Expr* e);
	Expr* recipExpr(Expr* e);
	Expr* addExpr(Expr* e1, Expr* e2);
	Expr* mulExpr(Expr* e1, Expr* e2);
	Expr* subExpr(Expr* e1, Expr* e2);
	Expr* divExpr(Expr* e1, Expr* e2);
	Expr* modExpr(Expr* e1, Expr* e2); // remainder

	// Declarations
	Decl* varDecl(Token id, Type* t);
	void finishVarDecl(Decl* d, Type* t);

	Decl* funcDecl(Token id, std::vector<Decl*> const& parms, Type* t);
	Decl* finishFuncDecl(Decl* d, Stmt* s);

	void enterScope() { scopeStack.emplace_back(); }

	void leaveScope() { scopeStack.pop_back(); }

	Scope* getScope() { return scopeStack.back(); }

	ScopeStack scopeStack;
};
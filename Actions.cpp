#include "Actions.hpp"

Expr* Actions::boolLiteral(Token const& tok) {

	if (tok.name == Token::true_kw) {
		return new Boolean_Expr(Type::bool_t, Value(true));

	} else {
		return new Boolean_Expr(Type::bool_t, Value(false));
	}
}

Expr* Actions::intLiteral(Token const& tok) {

	int i = std::stoi(std::string(tok.lexeme()));
	return new Int_Expr(Type::int_t, Value(i));
}

Expr* Actions::idExpr(Token const& tok) {

	Decl* decl = scopeStack.search(Symbol(tok.lexeme()));

	if(!decl) {

		throw std::runtime_error("Error matching declaration: No matching declaration.");
	}

	return new Identifier_Expr(decl->type, decl);
}

Expr* Actions::negateExpr(Expr* e) {
	return new Negate_Expr(e->type, e);
}

Expr* Actions::recipExpr(Expr* e) {
	return new Recip_Expr(e->type, e);
}

Expr* Actions::addExpr(Expr* e1, Expr* e2) {
	assert(e1->type == e2->type);
	return new Add_Expr(e1->type, e1, e2);
}

Expr* Actions::mulExpr(Expr* e1, Expr* e2) {
	assert(e1->type == e2->type);
	return new Mul_Expr(e1->type, e1, e2);
}

Expr* Actions::suExpr(Expr* e1, Expr* e2) {
	assert(e1->type == e2->type);
	return new Sub_Expr(e1->type, e1, e2);
}

Expr* Actions::divExpr(Expr* e1, Expr* e2) {
	assert(e1->type == e2->type);
	return new Div_Expr(e1->type, e1, e2);
}

Expr* Actions::modExpr(Expr* e1, Expr* e2) {
	assert(e1->type == e2->type);
	return new Mod_Expr(e1->type, e1, e2);
}

Decl* Actions::varDecl(Token id, Type* t) {

	Scope* scope = getScope();

	if(scope->search(id.getLexeme())) {
		throw std::runtime_error("Redefinition Error: Identifier already declared in scope.");
	}

	Decl* decl = new Obj_Decl(&id.name, t);

	scope->declare(decl);

	return decl;
}

void Actions::finishVarDecl(Decl* d, Expr* init) {

	Obj_Decl* decl = static_cast<Obj_Decl*>(d);
	decl->init = init;
}

Decl* Actions::funcDecl(Token id, std::vector<Decl*> const& parms, Type* t) {

	Scope* scope = getScope();

	if(scope->search(id.getLexeme())) {
		throw std::runtime_error("Redefinition Error: Function identifier already declared in scope.");
	}

	Type* funct = new Function_Type(parms);

	Fun_Decl* fun = new Fun_Decl(id.name, t, parms, new Block_Smt());

	scope->declare(fun);

	return fun;
}

Decl* Actions::finishFuncDecl(Decl* d, Stmt* s) {

	Fun_Decl* fun = static_cast<Fun_Decl*>(d);

	fun->body = s;

	return fun;
}
#include <iostream>

struct Name {

	Name(char const* s) : str(s)
	{}

	char const* str;
};

inline
void print(std::ostream& os, Name const* n){
	os << n->str;
}
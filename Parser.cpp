#include <iostream>

#include "Parser.hpp"

Parser::Parser(SymbolTable& stable, std::string const& ident)
	: lexer(stable, ident) {
		while(Token tok = lexer.getNextToken()) {
			toks.push_back(tok);
		}

		next = toks.data();
		last = next + toks.size();
}

Token Parser::consume() {

	assert(!isEof());
	Token tok = *next;
	next++;
	return tok;
}

Token Parser::match(Token::Name n) {

	if(next() == next.name) {
		return consume();
	}

	throw std::runtime_error("Syntax error: Incorrect token.");
	return Token();
}

Token Parser::require(Token::Name n) {
	assert(next() == next.name);
	return consume();
}

// ------------------------------------------------------- //
// Expressions

Expr* Parser::parseExpr() {

	return parseAssignExpr();
}

Expr* Parser::parseAssignExpr() {

	Expr* expr = parseAddExpr();

	if(match(Token::equals)) {
		return parseAssignExpr();
	}

	return expr;
}

Expr* Parser::parseAddExpr() {

	 parseMulExpr();

	 while(match(Token::plus) || match(Token::minus)) {
	 	parseMulExpr();
	 }
}

Expr* Parser::parseMulExpr() {

	Expr* lhs = parsePrefixExpr();

	while(true) {

		if(Token star = match(Token::star)) {
			Expr* rhs = parsePrefixExpr();
			lhs = actions.mulExpr(lhs, rhs);

		} if(Token star = match(Token::slash)) {
			Expr* rhs = parsePrefixExpr();
			lhs = actions.divExpr(lhs, rhs);

		} if(Token star = match(Token::percent)) {
			Expr* rhs = parsePrefixExpr();
			lhs = actions.modExpr(lhs, rhs);
		}

		break;
	}
}

Expr* Parser::parsePrefixExpr() {

	if(Token neg = match(Token::minus)) {
		Expr* ex = parsePrefixExpr();
		return actions.negExpr(ex);

	} if(Token rec = match(Token::slash)) {
		Expr* ex = parsePrefixExpr();
		return actions.recExpr(ex);
	}

	parsePostfixExpr();
}

Expr* Parser::parsePostfixExpr() {

	parsePrimaryExpr();

	while(true) {

		if(match(Token::lparen)) {

			while(peek().name != rparen) {

				if(Token tok = match(Token::int_literal)) {
					actions.intLiteral(tok);

				} if(Token tok = match(Token::identifier)) {
					actions.idExpr(tok);
				}
			}

			require(Token::rparen);

		} else {
			break;
		}
	}

	return nullptr;
}

Expr* Parser::parsePrimaryExpr() {

	if(Token tok = match(Token::int_literal)){
		return actions.intLiteral(tok);

	} if(Token tok = match(Toke::identifier)) {
		return actions.idExpr(tok);

	} if(match(Token::lparen)) {
		Expr* expr = parseExpr();
		expect(Token::rparen);
		return expr;
	}

	throw std::runtime_error("Error: Expected factor.");
}

// ------------------------------------------------------- //
// Declarstions

void Parser::parseProgram() {

	actions.enterScope();
	parseDecl();
	leaveScope();
}

Decl* Parser::parseDecl() {
	return parseLocalDecl();
}

Decl* Parser::parseLocalDecl() {
	return parseVarDecl();
}

Decl* Parser::parseVarDecl() {

	Token var = require(Token::var_kw);
	Token id = expect(Token::identifier);
	Token colon = expect(Token::colon);
	Type* t = parseType();

	Decl* decl = actions.varDecl(id, t);

	Token eq = expect(Token::equals);
	Expr* init = parseExpr();
	Token scol = expect(Token::semicolon);

	actions.finishVarDecl(decl, init);

	return var;
}

Decl* Parser::parseFuncDecl() {

	Token tok = require(Token::fun_kw);
	Token id = require(Token::identifier);
	Token lparen = require(Token::lparen);

	// funtion block scope
	actions.enterScope();

	std::vector<Decl*> params;
	while(next != Token::rparen) {
		params.push_back(parseDecl);
	}

	Token rparen = require(Token::rparen);

	Token arrow = require(Token::arrow);
	Type* t = parseType();

	Decl* func = actions.funcDecl(id, params, t);

	Stmt* body = parseStmt();
	actions.finishFuncDecl(func, body);

	actions.leaveScope();

	return func;
}

// ------------------------------------------------------- //
// Statements

Stmt* Parser::parseStmt() {

	switch(next().name) {

		case Token::semicolon:
			return parseEmptyStmt();

		case Token::lbrace:
			return parseBlockStmt();

		case Token::if_kw:
			return parseIfStmt();

		case Token::while_kw:
			return parseWhileStmt();

		case Token::break_kw:
			return parseBreakStmt();

		case Token::continue_kw:
			return parseContinueStmt();

		case Token::return_kw:
			return parseReturnStmt();

		case Token::ref_kw:
			return parseDeclStmt();
		case Token::var_kw:
			return parseDeclStmt();

		default:
			return parseExprStmt();
	}
}

Stmt* Parser::parseEmptyStmt() {
	require(Token::semicolon);
	return nullptr;
}

Stmt* Parser::parseBlockStmt() {

	require(Token::lbrace);

	actions.enterScope();

	std::vector<Stmt*> stmts;

	while(next().name != Token::rparen) {

		Stmt* s = parseStmt();
		stmts.push_back(s);
	}

	require(Token::rbrace);

	return new Block_Stmt(std::move(stmts));
}

Stmt* Parser::parseIfStmt() {

	require(Token::if_kw);
	require(Token::lparen);

	Expr* cond = parseExpr();

	require(Token::rparen);

	Stmt* ifs = parseStmt();

	expect(Token::else_kw);

	Stmt* els = parseStmt();

	return new If_Stmt(cond, ifs, els);
}

Stmt* Parser::parseWhileStmt() {

	require(Token::while_kw);
	require(Token::lparen);

	Expr* cond = parseExpr();

	expect(rparen);

	Stmt* body = parseStmt();

	return new While_Stmt(cond, body);
}

Stmt* Parser::parseBreakStmt() {

	require(Token::break_kw);
	require(Token::semicolon);
	return new Break_Stmt();
}

Stmt* Parser::parseContinueStmt() {

	require(Token::continue_kw);
	require(Token::semicolon);
	return new Continue_Stmt();
}

Stmt* Parser::parseReturnStmt() {

	require(Token::return_kw);

	Expr* val = parseExpr();

	require(Token::semicolon);

	return new Return_Stmt(val);
}

Stmt* Parser::parseDeclStmt() {

	Decl* decl = parseLocalDecl();
	return new Decl_Stmt(decl);
}

Stmt* Parser::parseExprStmt() {

	Expr* e = parseExpr();

	require(Token::semicolon);

	return new Expr_Stmt(e);
}
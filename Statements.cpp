#include "Statements.hpp"
#include "Expressions.hpp"
#include "Declarations.hpp"
#include "Value.hpp"

#include <iostream>

static void print_Expr(std::ostream& os, Expr_Stmt const* s) {
	print(os, s->expr);
}

static void print_Decl(std::ostream& os, Decl_Stmt const* s) {
	print(os, s->decl);
}

static void print_Ret(std::ostream& os, Return_Stmt const* s) {
	os << "return ";
	print(os, s->value);
}

static void print_While(std::ostream& os, While_Stmt const* s) {

	os << "while ";
	print(os, s->expr);
	os << " do ";
	print(os, s->stmnt);
}

static void print_If(std::ostream& os, If_Stmt const* s) {

	os << "if ";
	print(os, s->expr);
	os << " then ";
	print(os, s->stmnts[0]);
	os << " else ";
	print(os, s->stmnts[1]);	
}

static void print_Block(std::ostream& os, Block_Stmt const* s) {

	os << "{/n";

	for(Stmt* st : s->stmnts) {
		os << '/t';
		print(os, st);
		os << '/n';
	}

	os << "}";
}

void print(std::ostream& os, Stmt const* s) {

	switch(s->kind) {

		case Stmt::break_stmt:
			os << "break";

		case Stmt::cont_stmt:
			os << "continue";

		case Stmt::ret_stmt:
			return print_Ret(os, static_cast<Return_Stmt const*>(s));

		case Stmt::expr_stmt:
			return print_Expr(os, static_cast<Expr_Stmt const*>(s));

		case Stmt::decl_stmt:
			return print_Decl(os, static_cast<Decl_Stmt const*>(s));

		case Stmt::while_stmt:
			return print_While(os, static_cast<While_Stmt const*>(s));

		case Stmt::if_stmt:
			return print_If(os, static_cast<If_Stmt const*>(s));

		case Stmt::block_stmt:
			return print_Block(os, static_cast<Block_Stmt const*>(s));
	}
}

std::ostream& operator<<(std::ostream& os, Stmt const& s) {
	print(os, &s);
	return os;
}
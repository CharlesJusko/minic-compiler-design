#include "Token.hpp"
#include "Lexer.hpp"
#include "Actions.hpp"

#include <cassert>
#include <vector>

class SymbolTable;
class Type;
class Expr;
class Stmt;
class Decl;

struct Parser {

	Parser(SymbolTable& stable, std::string const& ident);

	bool isEof() const { return next == last; }

	const Token& peek() const { return *next; }

	Token::Name next() const { return peek().name; }

	Token consume();

	Token match(Token::Name n);

	Token expect(Token::Name n);

	Token require(Token::Name n);

	void parse();

	// Expression Parsers
	Expr* parseExpr();

	Expr* parseAssignExpr();

	Expr* parseAddExpr();
	// '+', '-'

	Expr* parseMulExpr();
	// '*', '/', '%'

	Expr* parsePrefixExpr();
	// '-'(Negate), '/'(Recip)

	Expr* parsePostfixExpr();
	// args list

	Expr* parsePrimaryExpr();
	// literals

	// Type Parsers
	Type* parseType();

	// Declaration Parsers
	void parseProgram();

	Decl* parseDecl();

	Decl* parseLocalDecl();

	Decl* parseFuncDecl();

	Decl* parseVarDecl();

	// Statement Parsers
	Stmt* parseStmt();

	Stmt* parseBlockStmt();

	Stmt* parseIfStmt();

	Stmt* parseWhileStmt();

	Stmt* parseBreakStmt();

	Stmt* parseContinueStmt();

	Stmt* parseReturnStmt();

	Stmt* parseDeclStmt();

	Stmt* parseExprStmt();

	Stmt* parseEmptyStmt();

	Lexer lexer;

	std::vector<Token> toks;

	Token* next;

	Token* last;

	Actions actions;
};